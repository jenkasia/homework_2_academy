"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fightersView_1 = require("./components/fightersView");
const fightersService_1 = require("./services/fightersService");
class App {
    constructor() {
        this.startApp();
    }
    async startApp() {
        try {
            App.loadingElement.style.visibility = 'visible';
            const fighters = await fightersService_1.fighterService.getFighters();
            const fightersElement = fightersView_1.createFighters(fighters);
            App.rootElement.appendChild(fightersElement);
        }
        catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load data';
        }
        finally {
            App.loadingElement.style.visibility = 'hidden';
        }
    }
}
App.rootElement = document.getElementById('root');
App.loadingElement = document.getElementById('loading-overlay');
exports.default = App;
//# sourceMappingURL=app.js.map