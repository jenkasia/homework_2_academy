import { createElement } from '../helpers/domHelper';
export function createFighterPreview(fighter, position) {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });
    const name = createTextElement('Name', fighter.name);
    const health = createTextElement('Health', String(fighter.health));
    const attack = createTextElement('Attack', String(fighter.attack));
    const defense = createTextElement('Defense', String(fighter.defense));
    const img = createFighterImage(fighter);
    fighterElement.append(name, health, attack, defense, img);
    // todo: show fighter info (image, name, health, etc.)
    return fighterElement;
}
function createTextElement(fieldName, info) {
    const element = document.createElement('div');
    element.classList.add('fighter-preview__item');
    element.innerHTML = `<p class = "fighter-preview__key">${fieldName}:</p>
  <p class = "fighter-preview__value">${info}</p>`;
    return element;
}
export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name,
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });
    return imgElement;
}
