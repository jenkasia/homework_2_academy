import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';
export function showWinnerModal(fighter) {
    const title = `Winner player is ${fighter.name}`;
    const bodyElement = document.createElement('div');
    bodyElement.append(createElement({
        tagName: 'img',
        className: 'modal__photo',
        attributes: {
            src: fighter.source,
            title: fighter.name,
            alt: fighter.name,
        },
    }));
    function startAgainHandler() {
        return function () {
            const rootElemnt = document.getElementById('root');
            rootElemnt.innerHTML = '';
            return new App();
        };
    }
    showModal({ title, bodyElement, onClose: startAgainHandler() });
    // call showModal function
}
