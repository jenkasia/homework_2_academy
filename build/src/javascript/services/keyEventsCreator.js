import { getDamage } from '../components/fight';
export function fighterActionsHandler(fightInfo) {
    return new Promise((resolve) => {
        let pressed = new Set();
        function removeKeyPressHandler(fighter) {
            document.removeEventListener('keyup', keyUp);
            document.removeEventListener('keydown', keyPressHandler);
            return resolve(fighter);
        }
        function keyUp(event) {
            pressed.delete(event.code);
            switch (event.code) {
                //Figter 1 Attack KeyA
                case fightInfo.firstFighter.blockKey:
                    removeBlock(fightInfo.firstFighter);
                    break;
                //Figter 2 Deffence KeyL
                case fightInfo.secondFighter.blockKey:
                    removeBlock(fightInfo.secondFighter);
                    break;
            }
        }
        function dieCheck(fightInfo) {
            if (fightInfo.firstFighter.health <= 0) {
                diedFighter(fightInfo.firstFighter);
                return removeKeyPressHandler(fightInfo.secondFighter.fighterInstance);
            }
            else if (fightInfo.secondFighter.health <= 0) {
                diedFighter(fightInfo.secondFighter);
                return removeKeyPressHandler(fightInfo.firstFighter.fighterInstance);
            }
        }
        function keyPressHandler(event) {
            pressed.add(event.code);
            switch (event.code) {
                //Figter 1 Attack KeyA
                case fightInfo.firstFighter.hitKey:
                    if (!fightInfo.firstFighter.block) {
                        attackFromTo(fightInfo.firstFighter, fightInfo.secondFighter);
                    }
                    break;
                //Figter 1 Deffence KeyD
                case fightInfo.firstFighter.blockKey:
                    setBlock(fightInfo.firstFighter);
                    break;
                //Figter 2 Attack KeyJ
                case fightInfo.secondFighter.hitKey:
                    if (!fightInfo.secondFighter.block) {
                        attackFromTo(fightInfo.secondFighter, fightInfo.firstFighter);
                    }
                    break;
                //Figter 2 Deffence KeyL
                case fightInfo.secondFighter.blockKey:
                    setBlock(fightInfo.secondFighter);
                    break;
            }
            fightInfo.firstFighter.criticalAvailable && checkCriticalPresser(fightInfo.firstFighter, pressed)
                ? makeCryticalAttack(fightInfo.firstFighter, fightInfo.secondFighter)
                : 0;
            fightInfo.secondFighter.criticalAvailable && checkCriticalPresser(fightInfo.secondFighter, pressed)
                ? makeCryticalAttack(fightInfo.secondFighter, fightInfo.firstFighter)
                : 0;
            return dieCheck(fightInfo);
        }
        document.addEventListener('keydown', keyPressHandler);
        document.addEventListener('keyup', keyUp);
    });
}
function blockFighter(className) {
    let element = document.querySelector(className);
    element.style.opacity = '0.5';
}
async function fightViever(className) {
    const shiftValue = document.body.clientWidth * 0.35;
    const shiftPlayer = className.includes('left') ? shiftValue : shiftValue * -1;
    document.querySelector(className).style.opacity = '1';
    document.querySelector(className).style.transform = `translateX(${shiftPlayer}px)`;
    await setTimeout(() => {
        document.querySelector(className).style.transform = 'translateX(0)';
    }, 250);
}
function updateHealth(fighter) {
    const healtToFixed = Number(fighter.health.toFixed(10));
    const fighterHealth = (healtToFixed / fighter.fighterInstance.health) * 100;
    const healthColor = changeHealthColor(fighterHealth);
    fighter.healthIndicator.style.width = `${fighterHealth > 0 ? fighterHealth : 0}%`;
    fighter.healthIndicator.style.backgroundColor = healthColor;
}
function attackFromTo(attacker, deffender, crytical = false) {
    attacker.block = false;
    let damage = 0;
    if (crytical) {
        damage = attacker.fighterInstance.attack * 2;
        deffender.block = false;
        document.querySelector(deffender.fighterClass).style.opacity = '1';
    }
    else if (!deffender.block) {
        damage = getDamage(attacker.fighterInstance, deffender.fighterInstance);
    }
    deffender.health = damage > 0 ? deffender.health - damage : deffender.health;
    updateHealth(deffender);
    fightViever(attacker.fighterClass);
}
function setBlock(fighter) {
    fighter.block = true;
    blockFighter(fighter.fighterClass);
}
function removeBlock(fighter) {
    document.querySelector(fighter.fighterClass).style.opacity = '1';
    fighter.block = false;
}
function diedFighter(deadFighter) {
    deadFighter.side == 'left'
        ? (document.querySelector(deadFighter.fighterClass).style.transform =
            'rotate(-90deg) translateX(-100px)')
        : (document.querySelector(deadFighter.fighterClass).style.transform =
            'rotate(90deg) translateX(100px)');
}
function changeHealthColor(health) {
    if (health > 50) {
        const redValue = Math.ceil(3.54 * (100 - Math.ceil(health) + 6));
        return `rgb(${redValue}, 183, 0)`;
    }
    else {
        let greenValue = Math.ceil(3.66 * Math.floor(health));
        greenValue = greenValue > 0 ? greenValue : 0;
        return `rgb(183, ${greenValue}, 0)`;
    }
}
function makeCryticalAttack(attacker, deffender) {
    const startValueDeffenderBlock = deffender.block;
    attacker.criticalAvailable = false;
    attacker.cryticalHitIndicator.style.width = '0%';
    const cryticalHitUpdater = setInterval(() => (attacker.cryticalHitIndicator.style.width = `${parseFloat(attacker.cryticalHitIndicator.style.width) + 1 / 10}%`), 10);
    setTimeout(() => {
        attacker.criticalAvailable = true;
        clearInterval(cryticalHitUpdater);
        attacker.cryticalHitIndicator.style.width = '100%';
    }, 10000);
    const cryticalPower = attacker.fighterInstance.attack * 2;
    attackFromTo(attacker, deffender, true);
    let audio = new Audio(deffender.criticalHitAudio);
    setTimeout(() => {
        startValueDeffenderBlock ? setBlock(deffender) : 0;
        audio.play();
    }, 200);
}
function checkCriticalPresser(attacker, clickedButtons) {
    for (let code of attacker.criticalHitControls) {
        if (!clickedButtons.has(code)) {
            return 0;
        }
    }
    return true;
}
