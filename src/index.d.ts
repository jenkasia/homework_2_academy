declare module '*.png';

interface Fighter {
  _id: string;
  name: string;
  health: number;
  attack: number;
  defense: number;
  source: string;
}

interface FighterInfo {
  name: string;
  health: number;
  attack: number;
  defense: number;
}

interface IFighterAndInfo {
  side: string;
  fighterInstance: Fighter;
  block: boolean;
  criticalAvailable: boolean;
  health: number;
  hitKey: string;
  blockKey: string;
  criticalHitControls: Array<string>;
  healthIndicator: HTMLElement;
  cryticalHitIndicator: HTMLElement;
  fighterClass: string;
  criticalHitAudio: string;
}

export { Fighter, FighterInfo, IFighterAndInfo };
