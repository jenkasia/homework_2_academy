import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { Fighter, FighterInfo } from '../../index';

export function renderArena(selectedFighters: Fighter[]) {
  const root = document.getElementById('root')!;
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  const [firstFighter, secondFighter] = selectedFighters;
  let waitForWinner = fight(firstFighter, secondFighter);
  // console.log(a)
  waitForWinner.then((fighter) => {
    showWinnerModal(fighter);
  });
  // todo:
  // - start the fight
  // - when fight is finished show winner
}

function createArena(selectedFighters: Fighter[]) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(selectedFighters[0], selectedFighters[1]);
  const fighters = createFighters(selectedFighters[0], selectedFighters[1]);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: Fighter, rightFighter: Fighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: Fighter, position: string) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const superHit = createElement({ tagName: 'div', className: 'arena___super-hit-indicator' });
  const bar = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` },
  });
  const superHitBar = createElement({
    tagName: 'div',
    className: 'arena___super-hit-bar',
    attributes: { id: `${position}-super-hit-indicator` },
  });

  fighterName.innerText = name;
  indicator.append(bar);
  superHit.append(superHitBar);
  container.append(fighterName, indicator, superHit);

  return container;
}

function createFighters(firstFighter: Fighter, secondFighter: Fighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: Fighter, position: string) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
