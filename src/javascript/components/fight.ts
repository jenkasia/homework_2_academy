import { fighters } from './../helpers/mockData';
import { controls } from '../../constants/controls';
import { fighterActionsHandler } from '../services/keyEventsCreator';
import { Fighter, IFighterAndInfo } from '../../index';

export async function fight(firstFighter: Fighter, secondFighter: Fighter): Promise<Fighter> {
  console.log(firstFighter, secondFighter);

  interface IfightInfo {
    [key: string]: IFighterAndInfo;
  }

  let fightInfo: IfightInfo = {
    firstFighter: {
      side: 'left',
      fighterInstance: firstFighter,
      block: false,
      criticalAvailable: true,
      health: firstFighter.health,
      hitKey: controls.PlayerOneAttack,
      blockKey: controls.PlayerOneBlock,
      criticalHitControls: controls.PlayerOneCriticalHitCombination,
      healthIndicator: document.getElementById('left-fighter-indicator')!,
      cryticalHitIndicator: document.getElementById('left-super-hit-indicator')!,
      fighterClass: '.arena___left-fighter',
      criticalHitAudio: 'resources/fighter1.mp3',
    },

    secondFighter: {
      side: 'right',
      fighterInstance: secondFighter,
      block: false,
      criticalAvailable: true,
      health: secondFighter.health,
      hitKey: controls.PlayerTwoAttack,
      blockKey: controls.PlayerTwoBlock,
      criticalHitControls: controls.PlayerTwoCriticalHitCombination,
      healthIndicator: document.getElementById('right-fighter-indicator')!,
      cryticalHitIndicator: document.getElementById('right-super-hit-indicator')!,
      fighterClass: '.arena___right-fighter',
      criticalHitAudio: 'resources/fighter2.mp3',
    },
  };

  return new Promise<Fighter>((resolve) => {
    resolve(fighterActionsHandler(fightInfo));
  });
}

export function getDamage(attacker: Fighter, defender: Fighter) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  let damage = hitPower - blockPower;
  damage = damage > 0 ? damage : 0;
  return damage;
}

export function getHitPower(fighter: Fighter) {
  const hitMultiplyer = actionMultiplier();
  const power = fighter.attack * hitMultiplyer;
  return power;
  // return hit power
}

export function getBlockPower(fighter: Fighter) {
  const blockMultiplyer = actionMultiplier();
  const power = fighter.defense * blockMultiplyer;
  return power;
}

function actionMultiplier() {
  return Math.random() + 1;
}
