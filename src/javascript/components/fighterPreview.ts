import { createElement } from '../helpers/domHelper';
import { Fighter } from '../../index';

export function createFighterPreview(fighter: Fighter, position: string): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const name: HTMLElement = createTextElement('Name', fighter.name);
  const health: HTMLElement = createTextElement('Health', String(fighter.health));
  const attack: HTMLElement = createTextElement('Attack', String(fighter.attack));
  const defense: HTMLElement = createTextElement('Defense', String(fighter.defense));
  const img: HTMLElement = createFighterImage(fighter);

  fighterElement.append(name, health, attack, defense, img);
  // todo: show fighter info (image, name, health, etc.)
  return fighterElement;
}

function createTextElement(fieldName: string, info: string): HTMLElement {
  const element: HTMLElement = document.createElement('div');
  element.classList.add('fighter-preview__item');
  element.innerHTML = `<p class = "fighter-preview__key">${fieldName}:</p>
  <p class = "fighter-preview__value">${info}</p>`;
  return element;
}

export function createFighterImage(fighter: Fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
