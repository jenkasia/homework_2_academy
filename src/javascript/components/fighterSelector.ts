import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { Fighter, FighterInfo } from '../../index';
const versusImg = require('../../../resources/versus.png');

export function createFightersSelector(): Function {
  let selectedFighters: Fighter[] = [];

  return async (event: MouseEvent, fighterId: string): Promise<void> => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    // eslint-disable-next-line no-extra-boolean-cast
    const secondFighter = Boolean(playerOne) ? playerTwo ?? fighter : playerTwo;
    const target = (<HTMLElement>event.target).closest('div');
    if (!playerOne && target !== null) {
      target.style.border = '5px solid red';
    } else if (!playerTwo && target !== null) {
      target.style.border = '5px solid blue';
    }
    selectedFighters = [firstFighter, secondFighter];
    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId: string): Promise<Fighter> {
  let fighterInfo = await fighterService.getFighterDetails(fighterId);

  fighterDetailsMap.set(fighterId, fighterInfo);
  return fighterInfo;
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
}

function renderSelectedFighters(selectedFighters: Fighter[]) {
  const fightersPreview = document.querySelector('.preview-container___root')!;
  const [playerOne, playerTwo] = selectedFighters;
  fightersPreview.innerHTML = '';
  const firstPreview = createFighterPreview(playerOne, 'left');
  try {
    const secondPreview = createFighterPreview(playerTwo, 'right');
    const versusBlock = createVersusBlock(selectedFighters);
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
  } catch {
    const versusBlock = createVersusBlock(selectedFighters);
    fightersPreview.append(firstPreview, versusBlock);
  }
}

function createVersusBlock(selectedFighters: Fighter[]) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: Fighter[]) {
  renderArena(selectedFighters);
}
