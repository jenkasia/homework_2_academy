import { createElement } from '../helpers/domHelper';
import { createFightersSelector, getFighterInfo } from './fighterSelector';
import { Fighter, FighterInfo, IFighterAndInfo } from '../../index';

export function createFighters(fighters: Array<Fighter>) {
  const selectFighter = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: Fighter, selectFighter: Function) {
  let fighterInfo: HTMLElement;
  new Promise<HTMLElement>((resolve) => {
    resolve(fighterInfoCreator(fighter._id));
  }).then((fighter) => {
    fighterInfo = fighter;
  });
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  fighterElement.addEventListener('mouseover', function () {
    fighterInfo ? fighterElement.append(fighterInfo) : 0;
  });

  const imageElement = createImage(fighter);
  fighterElement.addEventListener('mouseout', function () {
    try {
      document.querySelector('.fighter___fighter-info')!.remove();
    } catch {
      return 0;
    }
  });

  const onClick = (event: MouseEvent) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: Fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes,
  });

  return imgElement;
}

async function fighterInfoCreator(id: string): Promise<HTMLElement> {
  const fighter: FighterInfo = await getFighterInfo(id);
  const { name, attack, defense, health } = fighter;
  const fighterBlock = createElement({
    tagName: 'div',
    className: 'fighter___fighter-info',
  });

  const fighterName = document.createElement('h4');
  fighterName.classList.add('fighter___fighter-info-item');
  fighterName.innerHTML = `${name}`;
  const fighterAttack = document.createElement('p');
  fighterAttack.classList.add('fighter___fighter-info-item');
  fighterAttack.innerHTML = `Attack: ${attack}`;
  const fighterDefense = document.createElement('p');
  fighterDefense.classList.add('fighter___fighter-info-item');
  fighterDefense.innerHTML = `Defense: ${defense}`;
  const fighterHealth = document.createElement('p');
  fighterHealth.classList.add('fighter___fighter-info-item');
  fighterHealth.innerHTML = `Health: ${health}`;
  fighterBlock.append(fighterName, fighterAttack, fighterDefense, fighterHealth);
  return fighterBlock;
}
