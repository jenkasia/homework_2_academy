interface StringMap {
  [key: string]: string;
}

interface Element {
  tagName: string;
  className: string;
  attributes?: StringMap;
}

export function createElement({ tagName, className, attributes = {} }: Element) {
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames: Array<string> = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
