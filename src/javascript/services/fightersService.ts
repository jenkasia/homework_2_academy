import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../../index';

class FighterService {
  async getFighters(): Promise<any> {
    // eslint-disable-next-line no-useless-catch
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<any> {
    // eslint-disable-next-line no-useless-catch
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
    // todo: implement this method
    // endpoint - `details/fighter/${id}.json`;
  }
}

export const fighterService = new FighterService();
